echo "Start deploy to docker.hub"
docker-compose build
docker login --username your_user_name_docker_hub --password your_user_password_docker_hub
docker push your_user_repo_docker_hub
echo "Project deployed to docker.hub"