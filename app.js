'use strict';

const express = require('express');

const app = express();
const bodyParser = require('body-parser');

app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));

const morgan = require('morgan');
app.use(morgan(':method :url :status :response-time'));

//CORS config
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,PATCH');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});
app.use(function (req, res, next) {
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

// Routing
app.use('/auth', require('./app/api/auth/auth.controller'));

//apidoc
app.use(express.static('apidoc'));

const server = app.listen(process.env.PORT || 3000);
module.exports = server;
