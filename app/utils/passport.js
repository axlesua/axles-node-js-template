'use strict';

const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const configManager = require('../config/config.manager');

module.exports.createAuthToken = (user, token) => {
    const tokenSecret = token || configManager.getTokenSecret();
    const tokenPayload = {
        userId: user.id
    };
    const expiresIn = 24 * 60 * 60;
    return jwt.sign(tokenPayload, tokenSecret, {expiresIn: expiresIn})
};

module.exports.extractAuthToken = (authToken, token) => {
    const tokenSecret = token || configManager.getTokenSecret();
    return new Promise((resolve, reject) => {
        jwt.verify(authToken, tokenSecret, (err, decoded) => {
            if (err) {
                return reject(err)
            }
            return resolve(decoded)
        })
    })
};

module.exports.encryptPassword = (password) => {
    try {
        return crypto.createHash('sha1').update(password).digest('hex')
    } catch (err) {
        throw "error"
    }
};
