'use strict';

const multer = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const configManager = require('../config/config.manager');

aws.config.update({
    secretAccessKey: configManager.getAwsClientSecret(),
    accessKeyId: configManager.getAwsClientId(),
    region: configManager.getAwsRegion()
});

const s3 = new aws.S3();

const upload = function(path, fileName) {
    return multer({
        storage: multerS3({
            s3: s3,
            bucket: configManager.getAwsBucket(),
            contentType: function (req, file, cb) {
                cb(null, "image/jpg")
            },
            acl: 'public-read',
            metadata: function (req, file, cb) {
                cb(null, {fieldName: file.fieldname});
            },
            key: function (req, file, cb) {
                cb(null, path + fileName);
            }
        })
    });
};

module.exports = upload;
