'use strict';

module.exports.isValidPassword = (password) => {
    return password && password.length >= 8;
};
