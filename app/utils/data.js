//Roles
const superAdmin = "SUPER_ADMIN";
const companyMasterUser = "COMPANY_MASTER_USER";
const companyUser = "COMPANY_USER";
const companyAdmin = "COMPANY_ADMIN";
//Job status
const jobStatusNew = "JOB_STATUS_NEW";
const jobStatusInProgress = "JOB_STATUS_IN_PROGRESS";
const jobStatusCompleted = "JOB_STATUS_COMPLETED";
const jobStatusArchive = "JOB_STATUS_ARCHIVE";
const jobStatuses = [jobStatusNew, jobStatusInProgress, jobStatusCompleted, jobStatusArchive];

module.exports = class Data {
    static get roleSuperAdmin() {
        return superAdmin;
    }

    static get roleCompanyMasterUser() {
        return companyMasterUser;
    }

    static get roleCompanyUser() {
        return companyUser;
    }

    static get roleCompanyAdmin() {
        return companyAdmin;
    }

    static jobStatusNew() {
        return jobStatusNew
    }

    static jobStatusInProgress() {
        return jobStatusInProgress
    }

    static jobStatusCompleted() {
        return jobStatusCompleted
    }

    static jobStatusArchive() {
        return jobStatusArchive
    }

    static jobStatuses() {
        return jobStatuses
    }
};
