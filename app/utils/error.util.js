'use strict'

class ProjectError extends Error {
  constructor (message) {
    super(message);
    this.error = message
  }
}

module.exports.serverError = (err) => {
  const error = new ProjectError('SERVER_ERROR');
  if (err) error.message = err;
  error.code = 500;
  return error;
}

module.exports.notFound = (err) => {
  const error = new ProjectError('NOT_FOUND');
  if (err) error.message = err;
  error.code = 404;
  return error;
}

module.exports.badRequest = (err) => {
  var error = new ProjectError(err ? (err.message || 'BAD_REQUEST') : 'BAD_REQUEST');
  error.code = 400;
  return error;
}

module.exports.forbidden = (err) => {
  var error = new ProjectError(err ? (err.message || 'FORBIDDEN') : 'FORBIDDEN');
  if (err) error.message = err;
  error.code = 403;
  return error;
}

module.exports.unauthorised = (err) => {
  const error = new ProjectError('UNAUTHORISED');
  if (err) error.message = err;
  error.code = 401;
  return error;
}
