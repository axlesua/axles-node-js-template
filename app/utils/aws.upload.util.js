'use strict';

const aws = require('aws-sdk');
const configManager = require('../config/config.manager');

aws.config.update({
    secretAccessKey: configManager.getAwsClientSecret(),
    accessKeyId: configManager.getAwsClientId(),
    region: configManager.getAwsRegion()
});

module.exports.uploadBase64 = (path, fileString, callback) => {
    var s3Bucket = new aws.S3( { params: {Bucket: configManager.getAwsBucket()} } );
    var buf = new Buffer(fileString.replace(/^data:image\/\w+;base64,/, ""),'base64');
    var data = {
        Key: path,
        Body: buf,
        ContentEncoding: 'base64',
        ContentType: 'image/jpeg'
    };
    s3Bucket.putObject(data, function(err, data){
        if (err) {
            callback(null, err);
        } else {
            const url = "https://" + configManager.getAwsBucket() + ".s3." + configManager.getAwsRegion() + ".amazonaws.com/" + path;
            callback(url, null);
        }
    });
};
