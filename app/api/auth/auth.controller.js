'use strict';

const express = require('express');
const router = express.Router();
const authService = require('./auth.service');

router.post('/', async (req, res) => {
  try {
	  res.json(await authService.login(req.body))
  } catch (e) {
	  res.status(400).send(e);
  }
});

module.exports = router;
