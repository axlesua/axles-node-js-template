'use strict';

const authService = require('./auth.service');

module.exports.checkAuthToken = (req, res, next) => {
    authService.checkAccessToken(req, res, next, false);
};

module.exports.checkSuperAdmin = (req, res, next) => {
    authService.checkAccessToken(req, res, next, true);
};
