"use strict";

const db = require('../../model/db');
const passportUtil = require('../../utils/passport');
const errorUtil = require('../../utils/error.util');
const data = require("../../utils/data");

module.exports.login = async (dto) => {
    const email = dto.email;
    const password = dto.password;
    let user = await db.user.findOne({ where: {email: email}});
    if (!user) {
        throw errorUtil.badRequest(new Error("Incorrect login data"));
    }
    if (user.password !== passportUtil.encryptPassword(password)) {
        throw errorUtil.badRequest(new Error("Incorrect login data"));
    }
    const accessToken = passportUtil.createAuthToken(user);
    return {accessToken: accessToken};
};

module.exports.checkAccessToken = (req, res, next, checkSuperAdmin) => {
    const token = req.headers.authorization;
    if (!token) {
        res.status(401).json(errorUtil.unauthorised());
        return;
    }
    passportUtil.extractAuthToken(token)
        .then(decoded => {
            if (!decoded || !decoded.userId) {
                res.status(401).json(errorUtil.unauthorised());
                return;
            }
            db.user.findById(decoded.userId).then(user => {
                if (user) {
                    if (checkSuperAdmin) {
                        if (user.role === data.roleSuperAdmin) {
                            req.userId = user.id;
                            return next();
                        } else {
                            res.status(403).json(errorUtil.forbidden());
                        }
                    } else {
                        req.userId = user.id;
                        return next();
                    }
                } else {
                    res.status(403).json(errorUtil.forbidden());
                }
            });
        }).catch(reason => {
            res.status(401).json(errorUtil.unauthorised());
        });
};
