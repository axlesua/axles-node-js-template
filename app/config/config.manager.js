'use strict';

module.exports.getDbHost = () => {
    return process.env.DB_HOST;
};

module.exports.getDbPort = () => {
    return process.env.DB_PORT;
};

module.exports.getDbName = () => {
    return process.env.DB_NAME;
};

module.exports.getDbUser = () => {
    return process.env.DB_USER;
};

module.exports.getDbPassword = () => {
    return process.env.DB_PASSWORD;
};

module.exports.getTokenSecret = () => {
    return process.env.TOKEN_SECRET;
};

module.exports.getSuperAdminEmail = () => {
    return process.env.SUPER_ADMIN_EMAIL;
};

module.exports.getSuperAdminPassword = () => {
    return process.env.SUPER_ADMIN_PASSWORD;
};

module.exports.getSuperAdminFirstName = () => {
    return process.env.SUPER_ADMIN_FIRST_NAME;
};

module.exports.getSuperAdminLastName = () => {
    return process.env.SUPER_ADMIN_LAST_NAME;
};

module.exports.getAwsClientId = () => {
    return process.env.AWS_CLIENT_ID;
};

module.exports.getAwsClientSecret = () => {
    return process.env.AWS_CLIENT_SECRET;
};

module.exports.getAwsBucket = () => {
    return process.env.AWS_BUCKET;
};

module.exports.getAwsRegion = () => {
    return process.env.AWS_REGION;
};

module.exports.getAuthEmail = () => {
    return process.env.AUTH_EMAIL;
};

module.exports.getAuthEmailPassword = () => {
    return process.env.AUTH_EMAIL_PASSWORD;
};

module.exports.getTestUserEmail = () => {
    return process.env.TEST_USER_EMAIL;
};

module.exports.getTestUserPassword = () => {
    return process.env.TEST_USER_PASSWORD;
};