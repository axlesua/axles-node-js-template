'use strict';

module.exports.checkSuccess = (chai, err, res) => {
    chai.should().not.exist(err);
    res.should.have.status(200);
};

module.exports.checkError = (chai, err, res) => {
    chai.should().not.exist(err);
    this.checkObject(res);
    res.should.have.status(400);
    res.body.should.have.property('error');
    res.body.should.have.property('code');
};

module.exports.checkErrorUnauthorized = (chai, err, res) => {
    chai.should().not.exist(err);
    this.checkObject(res);
    res.should.have.status(401);
    res.body.should.have.property('error');
    res.body.should.have.property('code');
};

module.exports.checkObject = res => {
    res.body.should.be.an('object');
};

module.exports.checkArray = res => {
    res.body.should.be.an('array');
};

module.exports.checkString = res => {
    res.body.should.be.an('string');
};

module.exports.checkPagination = res => {
    res.body.should.have.property('rows');
    res.body.should.have.property('count');
};

module.exports.checkUser = user => {
    user.should.have.property('id');
    user.should.have.property('firstName');
    user.should.have.property('lastName');
    user.should.have.property('email');
    user.should.have.property('role');
    user.should.not.have.property('password');
};