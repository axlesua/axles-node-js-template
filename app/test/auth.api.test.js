'use strict';

const server = require('../../app');
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const configManager = require('../config/config.manager');
const chaiUtils = require('./chai.test.utils');

describe('Auth test', () => {
    it('POST /auth success', done => {
        chai.request(server)
            .post('/auth')
            .send({
                email: configManager.getTestUserEmail(),
                password: configManager.getTestUserPassword()
            })
            .end((err, res) => {
                chaiUtils.checkSuccess(chai, err, res);
                chaiUtils.checkObject(res);
                res.body.should.have.property('accessToken');
                done()
            })
    });

    it('POST /auth wrong data', done => {
        chai.request(server)
            .post('/auth')
            .send({
                email: "wrong",
                password: "wrong"
            })
            .end((err, res) => {
                chaiUtils.checkError(chai, err, res);
                done()
            })
    });

    it('POST /auth wrong password', done => {
        chai.request(server)
            .post('/auth')
            .send({
                email: configManager.getTestUserEmail(),
                password: "wrong"
            })
            .end((err, res) => {
                chaiUtils.checkError(chai, err, res);
                done()
            })
    });

    it('POST /auth wrong email', done => {
        chai.request(server)
            .post('/auth')
            .send({
                email: "wrong",
                password: configManager.getTestUserPassword()
            })
            .end((err, res) => {
                chaiUtils.checkError(chai, err, res);
                done()
            })
    });

});

