'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('Headers', 'order', Sequelize.TEXT);
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('Headers', 'order');
    },
};
