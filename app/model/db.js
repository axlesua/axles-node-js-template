"use strict";

const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const basename = path.basename(module.filename);
const data = require("../utils/data");
const passportUtil = require('../utils/passport');
const configManager = require('../config/config.manager');
const mysql = require('mysql2/promise');

const db = {};

async function initialize() {
    const connection = await mysql.createConnection({
        host: configManager.getDbHost(),
        port: configManager.getDbPort(),
        user: configManager.getDbUser(),
        password: configManager.getDbPassword()
    });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${configManager.getDbName()}\`;`);

    const sequelize = new Sequelize(configManager.getDbName(), configManager.getDbUser(), configManager.getDbPassword(), {
        host: configManager.getDbHost(),
        port: configManager.getDbPort(),
        dialect: 'mysql',
        operatorsAliases: false,
        logging: process.env.NODE_ENV !== 'test' ? console.log : false
    });

    fs.readdirSync(__dirname)
        .filter(function (file) {
            return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
        })
        .forEach(function (file) {
            let model = sequelize['import'](path.join(__dirname, file));
            //lowercase the name
            let name = model.name[0].toLowerCase() + model.name.substring(1);
            db[name] = model;
        });

    Object.keys(db).forEach(function (modelName) {
        if (db[modelName].associate) {
            db[modelName].associate(db);
        }
    });
    db.sequelize = sequelize;

    db.user.sync().then(function () {
        db.user.findOne({where: {role: data.roleSuperAdmin}}).then(user => {
            if (!user) {
                //create super admin if no one on db
                db.user.create({
                    email: configManager.getSuperAdminEmail(),
                    password: passportUtil.encryptPassword(configManager.getSuperAdminPassword()),
                    firstName: configManager.getSuperAdminFirstName(),
                    lastName: configManager.getSuperAdminLastName(),
                    role: data.roleSuperAdmin
                })
            }
        });
    }, function (err) {
        console.log(err)
    });
}

initialize();
module.exports = db;
