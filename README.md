# axles-node-js-template

To generate api doc run in terminal:
apidoc -i app/ -o apidoc/

To development with a docker:
docker-compose -f docker-compose.dev.yml up --build

Push to docker.hub:
sh docker-deploy.sh

To start app with a docker without development: 
docker-compose up

If you need rebuild docker image: 
docker-compose up --remove-orphans

To run migration:
app/model/migration: sequelize db:migrate

To kill all node processes:
sudo killall -9 node
